<?php

class Person extends CI_Model {
    public function all() {
        $this->load->database();
        return $this->db->from("persons")
            ->select("id")
            ->select("name")
            ->get()
            ->result();
    }
    
    public function create($name) {
        $this->load->database();
        $this->db->insert("persons", array("name" => $name));
        return $this->db->insert_id();
    }
    
    public function view($id) {
        $this->load->database();
        $p = $this->db
            ->from("persons")
            ->select("name")
            ->where("id", $id)
            ->limit(1)
            ->get()
            ->first_row();
        $p->id = $id;
        return $p;
    }
    
    public function edit($id, $name) {
        $this->load->database();
        $this->db->where("id", $id)->set("name", $name)->update("persons");
    }
    
    public function remove($id) {
        $this->load->database();
        $this->db->where("id", $id)->delete("persons");
    }
}