<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/persons.css" />
    </head>
    <body>
        <header><a href="/">Persons</a></header>
        <main>
            <?php $this->load->view($view); ?>
        </main>
    </body>
</html>