<h2><?=(isset($person))?'Edit':'Create'?> person.</h2>
<form method="POST">
    <p>
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" maxlength="32" required="true" value="<?=(isset($person))?html_escape($person->name):''?>"/>
    </p>
    
    <input type="submit" value="Save" /> 
</form>