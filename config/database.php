<?php

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => getenv("CI_CONFIG_DATABASE_HOSTNAME"),
	'username' => getenv("CI_CONFIG_DATABASE_USERNAME"),
	'password' => getenv("CI_CONFIG_DATABASE_PASSWORD"),
	'database' => getenv("CI_CONFIG_DATABASE_DATABASE"),
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => TRUE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);