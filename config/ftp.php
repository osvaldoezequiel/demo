<?php

$config['hostname'] = getenv("CI_CONFIG_FTP_HOSTNAME");
$config['username'] = getenv("CI_CONFIG_FTP_USERNAME");
$config['password'] = getenv("CI_CONFIG_FTP_PASSWORD");
$config['port']     = intval(getenv("CI_CONFIG_FTP_PORT"));
$config['passive']  = FALSE;
$config['debug']    = TRUE;