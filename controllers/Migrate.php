<?php

class Migrate extends CI_Controller {
    public function update() {
        $this->load->library('migration');
        if($this->migration->latest() === FALSE) {
            show_error($this->migration->error_string());
        } else {
        	echo "Fijado a la ultima version.\n";
        }
    }
    
    public function discard() {
        $this->load->library('migration');
        if($this->migration->version(0) === FALSE) {
            show_error($this->migration->error_string());
        } else {
        	echo "Se descartan todas las migraciones.\n";
        }
    }
    
    public function version($version) {
        $this->load->library('migration');
        if($this->migration->version($version) === FALSE) {
            show_error($this->migration->error_string());
        } else {
        	echo "Fijado a la version $version.\n";
        }
    }
    
    public function restart() {
        $this->load->library('migration');
        if($this->migration->version(0) === FALSE) {
            show_error($this->migration->error_string());
        } else {
        	if($this->migration->latest() === FALSE) {
            show_error($this->migration->error_string());
            } else {
            	echo "Reejecutadas todas las migraciones.\n";
            }
        }
    }
}